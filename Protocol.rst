.. _sec:protocol:

The Fathom Protocol
===================

.. _sec:architecture:

Architecture Overview
---------------------

This paragraph will introduce the three main components of a
fathom-assessment:
1. a *concept*, representing an assessable quality
2. the *concept-ontology*, which relates all concepts to each other.
3. *assessments* which draw individuals from the concepts to be assessors and,
upon a positive outcome, add new individuals to them.

.. _sec:concept:

Concept
~~~~~~~

*Concept* is an umbrella term to capture any kind of skill, quality,
piece of knowledge or fact that can be established about an individual.
Therefore, each concept :math:`C` has the following properties:

-  **Parent Concepts**: This is the set of concepts :math:`P` that
   :math:`C` is a subset of . For example, the concept ‘Math’ could be a
   parent of ‘Linear Algebra’. If there is no suitable parent, a concept
   can be located beneath the ‘mew’-concept - a specially designated
   concept with no skills associated to it and with no parents.

- **Connection-strength(s)**: For each parent :math:`p` :math:`\in P`, a concept
  :math:`C` denotes a connection strength :math:`c_p` from 0 to 1, specifying
  the degree of similarity or difference. All connection strengths collectively
  add up to 1, so that when an assessment needs to draw assessors from its
  parents, the connections strenghts can used to determine how many assessors
  should stem from each parent.

-  **Expiration time**: Concepts can specify expiration times
   :math:`e_c` to reflect that some skills become outdated, need to be
   maintained, or change over time. An example would be concepts related
   to taxation-laws, which are changed on a relatively frequent basis
   and where false or outdated information can lead to significant
   losses. Members who have been assessed in a concept longer ago than
   specified by the expiration time do not lose their certificates, but
   can no longer take part in the process of assessing others.

-  **Members**: a set of individuals who have passed an assessment in the
   concept in question or in one of its children

- **Weights**: For each member the concept stores a set of weights, a positive
  integer and date, corresponding to the latest assessment in the concept.
  Weights are used to probabilistically call a member to act as assessors in
  assessments [1]_.

- **Owner**: An ethereum address that controls what data is saved on the concept
  and which can move the concept around by changing its parents. [3]_.

.. _sec:concept-tree:

The Concept-Ontology
~~~~~~~~~~~~~~~~

As all concepts have at least one parent, the entirety of all concepts forms a
directed graph. The only concept without parents is the initial concept, the
:math:`mew`-concept. It does not present any particular subject or skill but
serves as the root-node of the graph and as parent to all new concepts that are
unrelated to the already existing ones. Thus, moving father away from the 'mew',
concepts become more specific.

This network of relationship among knowledge-communities is valuable when
sampling members to create a pool of potential assessors. If a concept has not
enough members to create a pool of assessors of sufficient size, additional
assessors will be drawn from the parent concepts.

As described in `Governance & Upgrades <./GovernanceAndUpgrade.html#sec:gov>`__
the set of concepts and its definitions will be changing over time, with concept
owners expected to update a concept's description and relation to parent
concepts (in coordination with its members) and with members cloning and
migrating their weights in case disagreements can not be resolved.

.. _sec:assgame:

The Assessment Game
~~~~~~~~~~~~~~~~~~~

An assessment is the process by which a jury of qualified individuals
(assessors) decides whether or not some candidate (assessee) fulfills
the necessary conditions to become a member of a concept. When
initiating an assessment in a concept, the assessee decides how many
assessors they want and how much they are willing to pay to each one of
them. That offer is forwarded to potential assessors (see
`setup <#sec:setup>`__ for drawing specifics) who must stake the offered
amount in order to accept. Thus, a market forms around assessments,
allowing the system to scale from easy to assess, and hence cheap,
concepts, to more involved, complicated, and hence expensive ones.

Upon completion of the assessment, assessors are paid the price offered
by the assessee and a proportion of their stake if they come to
consensus around the applicants skill. The proportion of the stake being
paid back is proportional to the assessor’s proximity to the average
score of the biggest cluster of scores and will also be added a portion
of the stake of dissenting assessors, should there be any (see
`payout <#sec:payout>`__ for details).

Also, the mechanism by which assessors log in their scores is designed
such that colluding assessors can double cross each other, thereby
creating a coordination problem in an adverserial environment, where the
only point of coordination (schelling point) left is a truthful
assessment. In case the majority vote of the assessors is positive, the
assessed candidate will get i) a score in the assessed concept, similar
to a grade in university or school, ii) a weight in the concept and iii)
a weight in all parent-concepts, proportionally reduced by their
respective connections strengths.

.. _sec:assprocess:

Assessment Process
------------------

A fathom assessment goes through five phases: A setup phase, where the
assessors are called from the concept tree, the assessment, where the
assessors determine the assessee’s skill, commit- and reveal-phases,
where the assessors log in their score and, at last, the calculation of
the result.

For each phase this section is gonna depict the choices of the involved
participants, their interactions and what happens if they deviate from
the protocol.

.. _sec:setup:

Setup
~~~~~

.. _sec:createAss:

Creating the assessment:
^^^^^^^^^^^^^^^^^^^^^^^^

Wanting to be certified in a concept :math:`C`, the assessee needs to
specify the following parameters:

1. A time period during which they would like the assessment to start and
   end (latest start and end time).

2. The number of assessors :math:`N_a` to be assessed by. While there is
   a minimum number of five assessors to guarantee a fair voting, the
   assessee might want to be assessed by a bigger number in order to
   receive a higher weight and higher chances to become assessors
   themselves (given that they end the assessment with a passing score).

3. The price :math:`cost_a` that each assessor will be paid.

.. _sec:sampling:

Calling assessors from the concept-tree:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A pool of potential assessors is created by probabilistically drawing
members from the concept and its parents. The selection of potential
assessors happens according to a tournament-selection of size 2,
starting at the assessed concept:

- Two members are picked at random and their weights are compared.

- The member with the higher weight is being added to the pool. In case of a
  draw, the member that was drawn first wins.

Thus each member has a chance of being called as assessor, whilst giving a
higher chance to those with higher weights.

To make it hard to predict who will be in the pool, no more than half of
the members of each concept can be called as assessors. Therefore, the
maximum number of tournaments per concept :math:`c` is limited by the
number of members in the concept. Specifically :math:`Y` is:

.. math::  Y = \min (N_{req}, \frac{m_c}{2}) 

with :math:`N_{req}` being the number of required assessors and
:math:`m_c` the number of members in the concept :math:`c`.

After :math:`Y` attempts, this selection process is repeated for each of the
:math:`n_{p_c}`\ ’s parent concepts of :math:`c`, using the connections
strengths to the different parents to determine how many members are
drawn from each parent.

The minimal size and the ideal size for the pool of assessor are subject to
parameters and will grow with the amount of members in the network.

.. _sec:confirm:

Assessors confirm by staking:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Each assessor that is being called, can decide to participate in it by
staking the offered price. Once the desired number of assessors has
confirmed, the assessment moves to the next stage. Assessors from the
pool self-select whether they think would be competent judges on the concept
in question. If so, they signal their intent to participate by staking
the offered price. More considerations why assessors would or wouldn’t
want to confirm are elaborated in the `incentive
section <./3-Security.html#sec:acceptAss>`__. If not enough assessors
can be found before the desired start-time of the assessment, the
assessment is cancelled and everybody who deposited collateral is
refunded.

.. _sec:assass:

Assessment of the candidate
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In a fathom-assessment there is no notion or form what constitutes a
test and the form or procedure of how candidates are evaluated is left
to each individual assessor. Ultimately, assessors express their verdict
of assessee’s skill as a number on a scale (e.g. between 0 and 100) -
with everything above half being considered a passing score.

Yet, what exactly defines a failing, passing or barely passing
assessment can be different for each concept as well and should be
agreed upon by the community. Moreover, the assessment could also be the
place to put up some sybil protection mechanism in the form of extra
requirements that make it hard to repeat an assessment (see
`sybil-attack <./3-Security.html#sec:sybil>`__ for more details on how
this could work).

.. _sec:commit:

Committing a Score
~~~~~~~~~~~~~~~~~~

Sending in a score follows the commit-reveal procedure common in
blockchain applications. Assessors signal that they have decided on a
score by concatenating it with a secret element, also referred to as
‘salt’ and submitting its hashed value (hash=sha3(score+salt)).

If any assessors fails to commit a score before the assessment ends
their stake is being burned. If, as a consequence, less assessors than
would be required for the minimum size of a viable assessment have
committed, the assessment is cancelled and everyone is being refunded.
Otherwise, the assessment progresses to the next stage.

.. _sec:stealReveal:

Steal and Reveal
~~~~~~~~~~~~~~~~

To end the assessment, the assessors reveal their verdict by submitting
their score and salt separately. Any assessor (or external person) who
knows about another assessor’s score and salt, can do so as well,
thereby stealing half of the assessor’s stake, burning the rest and
eliminating him/her from the assessment game. This prevents the
assessors from credibly guaranteeing each other their committment to
logging in a specific score, thus making it harder to collude.

While stealing is possible at all times after an assessor has committed
(even if others have not yet), revealing will only be possible after all
assessors have committed and a buffer period of 12 hours has elapsed.
The buffer ensures that there is time to challenge someone’s commit,
even if they waited until moments before the end of the assessment
period to send it in. Should any assessor fail to reveal, their stake is
burned and they are eliminated from the assessment game [2]_. If the
number of assessors decreases below the necessary minimum, the rest of
the participants is being refunded and the assessment ends without a
score.

.. _sec:outcome:

Determining the Outcome
~~~~~~~~~~~~~~~~~~~~~~~

In order for an assessment to result in a final score, one score must be
in consensus with enough other scores to form a 51% majority. Two scores
are considered to be in consensus if their difference is less than the
*consensus-distance* :math:`\phi`. If such a score :math:`s_{origin}`
exists, the final score :math:`s_{final}` is computed as the average of
all scores that are in consensus with :math:`s_{origin}`.

Should there be two scores with majorities of equal sizes, the one that
will result in a lower final score wins. If there is no point of
consensus, the assessment is considered invalid and all stakes are
burned. Otherwise the assessors’ payments will be computed as described
in the following section.

Also, in case the result :math:`s_{final}` is a passing score, the
assessee is registered as new member of the concept with a weight
:math:`w_i = s_{final} * N_{in}`. 

.. _sec:payout:

Payout of Assessors
~~~~~~~~~~~~~~~~~~~

Payments to assessors consist of two parts, their returned stake and the
assessee’s reward. Both are attributed differently, depending on whether
or not the assessor is inside out outside the majority cluster of
winning assessors.

Therefore, an assessor :math:`i`\ ’s distance :math:`dist_i` from the
final score :math:`s_{final}` is measured against the consensus range
:math:`\phi`:

.. math::  dist_i = \frac{\lvert s_i - s_{final} \rvert}{\phi} 

Outside assessors (:math:`1<dist_i<2`) only get back a part of their
stake, reduced linearly in relation to their distance from the final
cluster. Thus, an outside assessor :math:`j`\ ’s payout is computed as:

.. math::  payout_{out_j} = stake_j * \frac{(2 - dist_j)}{2} 

Inside assessors (:math:`dist_i <= 1`) get back their entire stake, any
stake that is not returned to outside assessors (distributed equally
among them) and a share of the assessee’s reward, proportional to their
proximity to the final score:

.. math::  payout_{in_i} = stake_i + \frac{1}{N_{in}} * \sum_j^{N_{out}} (stake_j-payout_{out_j}) + r_a * (1 - dist_i) 

,with :math:`N_{out}` and :math:`N_{in}` denoting the number of
assessors outside and inside the winning cluster and r_a being the
reward of the assessee.

Thus, the best case scenario for an assessor is to be inside the winning
cluster, close to the final score, with a large minority outside of it.

Any payout that is not returned to the assessors will be burned (if it
were redistributed, assessors could collude to cover a range of scores
and redistribute amongst them without any loss).

This figure summarizes the payout mechanism in a single graph:

|Figure 1: The payout mechanism: Three Assessors are inside the winning
cluster, two are outside of it.|

.. [1]
   Although it’s possible to repeat an assessment, only the result of
   the most recent assessment will be taken into account for the weight.

.. [2]
   This should be unlikely, as at that point assessors have nothing to
   lose but rewards to gain.

.. [3]
   While this is not specified by the protocol, we intend the data-field (a bytes array) to be
   the ultimate source of truth of what a concept is about. As the owner is just
   any ethereum address, this allows for a variety of governance schemes to be
   implemented. The owner can also transfer ownership to another address or set
   it to zero, in which case the data-field becomes immutable.


.. |Figure 1: The payout mechanism: Three Assessors are inside the winning cluster, two are outside of it.| image:: ./_static/img/payoutSummary.png

