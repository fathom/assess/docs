Introduction
============

Society functions on knowing what people can do. Everybody needs to be
able to communicate their skills to others in order to coordinate with them.
This used to be a social process within a local community, but society has
grown and largely outsourced that function to institutions. Today,
people need to communicate a greater diversity of skills and experiences
than ever before, over larger timescales and across geographic, cultural
and linguistic barriers. It is our belief that institutions do not
provide that service well, and that they will be increasingly ill-suited
to provide it in the future.

We seek to provide an alternative to institutional credentials.
Herein is specified a social protocol with economic incentives that
enables knowledge communities to define their own standards and individuals
to be assessed in those standards, resulting in credentials that are meaningful,
verifiable and durable.

.. _sec:problem:

Problem Statement
-----------------

The coupling of learning and assessment in current institutional models
is unscalable and creates a set of perverse incentives for both
educators and students. The bureaucracy of centralized institutions
makes them resource intensive and slow to adapt to changes. As a
consequence, they are only able to offer a small set of experiences
which default to those that can be mass-produced.

Because communicating one’s experiences is so essential in today’s
society, it is in an individual’s best interest to actively mold their
experiences towards what they can communicate instead of what
they can aspire to. Therefore, relying on instituions to be the arbiter
of people’s abilities has a chilling effect on societal progress.

.. _sec:introducing:

Introducing Fathom
------------------

Fathom is a protocol to create and assess meaningful credentials through
the consensus of knowledge communities.

It allows anyone to create a credential and anyone to be assessed in it. The
core process involves a jury of randomly assembled assessors with relevant
experience, as previously proven by the protocol, playing an ‘assessment-game’
in which they are economically incentivized such that an accurate assessment is
the `schelling point
<https://en.wikipedia.org/wiki/Focal_point_(game_theory)>`__.

The protocol makes no assumptions about what is being assessed. Instead,
it allows communities to form their own definitions and rules, and carry them
out collectively.

Implemented on a public blockchain, it will be possible to distribute
the work necessary for assessments to scale far beyond what institutions
are capable of.

Furthermore, blockchains can enable a credential-ecosystem that is truly inclusive,
accessible and extensible; one which is censor-resistant, durable, and
that leaves individuals in full control of their identities. Individuals are
able to acquire knowledge and accumulate experiences towards their unique aims,
while also shaping and strenghtening the network in their role as an assessor.

.. _sec:overview:

Overview
--------

The purpose of this document is to provide a formal specification of the
fathom-protocol. It is presented in the following parts:
First, `The Fathom Protocol <http://docs.fathom.network/en/latest/Protocol.html>`_
explains the various elements that make up the fathom network.
Then, the process of creating concepts and getting assessed in them
is presented step-by-step. Next,
`Concept Governance and Network Upgrades <http://docs.fathom.network/en/latest/Governance.html>`_
describes how forking (cloning) is used to collectively steer credential
definitions and to issue protocol upgrades. The third part,
`Security <http://docs.fathom.network/en/latest/Security.html>`_,
lays out the structure that incentivizes users to follow the protocol,
while making it economically disadvantageous to deviate from it.
Also presented here are some potential attack vectors and how they are mitigated.
The final part, `Vision <http://docs.fathom.network/en/latest/Vision.html>`_,
ties these threads to the goals and ambitions of the project.

.. _sec:contribution:

Contribution
------------

We believe in transparency and collaboration. We invite readers to engage with us
-- through comments, ideas, or by actively taking part in fathom's development.
Together, we seek to enable real-world applications of a wide variety to be built
on this new layer of trust and digital social relationships.

To learn more about our overall project roadmap, the infrastructure we are
building on top of the fathom protocol, our development process, and ways to
contribute, please check out our `repositories <https://gitlab.com/fathom>`__
and our `blog <https://fathom.network/blog/>`__.
