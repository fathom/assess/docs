.. Fathom documentation master file, created by
   sphinx-quickstart on Thu Sep  6 15:35:03 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fathom
==================================

.. warning::
    Fathom is still in the early stages of development. Here there be dragons. It's
    a great time to get involved though!

`Fathom <https://fathom.network>`_ is a cryptoeconomic protocol for creating globally recognized credentials.
It is implemented in a collection of smart contracts in `Solidity <https://solidity.readthedocs.io/>`_,
and deployed to the `Ethereum <https://ethereum.org>`_ blockchain.

Fathom aims to enable a new open learning ecosystem. On top of it you can create
new structures for learning: things like schools, tutoring systems, or
peer-to-peer learning networks.

.. note::
   Cryptoeconomics is a relatively new term and field. If you want to go deeper
   down the rabbit hole, check out `awesome-cryptoeconomics
   <https://github.com/L4ventures/awesome-cryptoeconomics>`_ from L4.


Why decentralized credentials? 
---------------------------------

Today most reputable credentials are issued by large institutions. This has led
to a credentialing system that is inaccessible to many, and that is slow to adapt
to changing realities. In this system, most can only communicate a small
part of their global skillset.

With fathom, it is possible to create credentials that capture any kind of knowledge
or skill. Moreover, conceiving of and defining credentials is no longer
the exclusive domain of centralized entities. Rather, it is something anyone can participate in.

As well, the process of earning credentials is less exclusionary and more meaningful.
The fathom protocol defines an assessment-game in which qualified assessors are economically
incentivized to come to a truthful evaluatation of an applicant's skill.

As such, any community in any field can create its own credentials and use them to
self-organize. Its members are empowered to communicate these skills and others to the outside world.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction
   Protocol
   Governance
   Security
   Vision
   Glossary
