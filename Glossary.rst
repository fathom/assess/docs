Glossary
=============

.. glossary::

   Assessor
     A user who validates whether one can be a member of a concept or not.

   Assessee
     The subject of an assessment who wants to earn a credential and join a `concept`.

   Staking
     The process by which assessors confirm for assessments by locking up a
     fixed amount of tokens. These tokens are either returned to them or burned,
     depending on their performance assessing.

   Concept
     Represents the shared knowledge, skillset, or other attribute that the users who have attained the concept have in commom. Examples could range from Calculus proficiency to English fluency to marathon completion.

   Weight
     Upon a successful assessment an individual earns a weight in a concept. This is calculated as a function of their score, the number of assessors in the largest cluster, and the time the assessment was taken.
