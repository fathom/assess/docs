.. _sec:Governance:

Concept Governance and Network Upgrades
========

This section introduces some of our considerations about how concepts will be
defined and changed over time in a decentralized manner that leaves no single
party with undue influence or decisionmaking power.

.. _sec:gov:

Concept Maintenance and Taxation
-------------------
    
Getting right what it means to pass or fail a credential will be a crucial to
allow assessors to come to consensus and is most likely very hard to get right
on the first attempt.

While second-layer solutions like offchain-registries are a possible
decentralized solution to this, we expect the data that is saved on the concept
to be the source of truth about what the concept is about and what makes an
assessee reach a pass or fail score.

Therefore, only the owner of the concept is allowed to change the concept's data
field. While this can be used in a centralized manner it also allows for
complicated governance schemes, e.g. if the owner were a smart contract that
requires changes to the concept definition to be signed off by a majority of
concept-members.

To incentivize the owners to keep a concept up-to-date and spend time and effort
to source input from concept members and the wider community, concept owners can
impose a 5% tax on all assessments run in the concept. That means that 5% of all
network tokens that are spent on this concept will be redirected to an address
of the owner's choice.

.. _sec:cloning:

Resolving Conflicts by Cloning
-------------------

    
In case there are conflicts about a concept-definition or the members feel like
the concept-taxation is unjustified, there is the possibility to "clone" a
concept.
    
Cloned concepts are just like regular concepts, i.e. all parameters can be
freely defined, except that they additionaly designate an "original"-concept.
Any members of the original concept can become members of the clone concept
without an assessment simply by forsaking their membership in the original.

'Migrating' members will have the same weight in the clone that they had in the original.

Thus owner are incentivized to only tax members if they actually provide value
to the community and unresolvable disagreements about credential-content can be
resolved by opting out of the definition without further costs.


.. _sec:upgrades:

Network Upgrades
-------------------

The cloning mechanism can also be used to issue protocol updates such as
changing the assessment-parameters or the assessor calling mechanism. 
To roll out such a change, a new network with a special set of cloning-rules
would be deployed. The new network will implement the updated protocol **and** allow
concepts to be clones of concept in the old-network.

Therefore, at any point in time, anyone can just deploy a new version of the
fathom-network and convince the community to adopt it by migrating their
memberships to the relevant concepts in the new network. To increase
transparency and allow community involvement, the first network will implement
a time-delay before changes can come into effect.






