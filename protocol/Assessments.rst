***********
Assessments
***********

Assessments are the meat of the fathom protocol. They're the process by which
individuals earn credentials and get added to concepts.

The high level overview is this:

1. Individuals pay tokens to the fathom network to start an assessment in a
   particular concept
2. Assessors are called from those who have previously been successfully
   assessed in that concept and related ones. Assessors from this pool can stake
   tokens to accept the assessment.
3. Once enough assessors have staked they all sectrely submit scores and once
   they've all submitted they reveal them
4. The final score is calculated based on the consensus of the individual ones
   and each assessor is rewarded or punished based on their distance from it. 

Below we'll go into more depth and look at the intricacies of each step.

.. contents:: Sections
   :local:
   :depth: 1

Initial Parameters
===================

Before an assessment can get started the `assessee` (the individual who wants a
credential) has to define some initial parameters:

concept
    The address of the `concept` they want to join
size
    The number of assessors they want to assess them (the minimum is five)
cost
    the number of tokens they are going to pay *per assessor*
confirmTime
    the latest time at which assessors shoudl confirm (i.e the *start* time of the assessment)
timeLimit
   how long the assessment should last, in seconds, starting from the `confirmTime`
           
In order to start the assessment the assessee needs to lock up `size` * `cost`
tokens.

Assessor Calling
=================

We only want *knowledgeable* assessors to be eligible for a particular
assessment and so we want to call them based on their previous performance
within the fathom protocol.

The most eligible people are those who have a high weight in the concept the
assessment is in so we call from them first.

Getting randomness
-------------------

Commiting & Revealing
=======================

Stealing
---------

Randomness from salts
----------------------

Calculating Scores & Rewarding
=================================
