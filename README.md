This serves as source of truth of our documentation about the protocol.
It will ultimately replace the whitepaper that is in our ```fathom/fathom.gitlab.io``` repo.

The master branch of this repo is served to: http://docs.fathom.network/en/latest/

## how to work on it

use python's pip to install
sphinx[https://docs.readthedocs.io/en/latest/intro/getting-started-with-sphinx.html#quick-start]
:

```
pip install sphinx
```

In the root directory, run 
```
make html
```

To look at the build docs, open ```_build/html/index.html_```.
